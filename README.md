# IST-Blood-Donation-App
## Login with phone number
![Screenshot](login-by-phone-number.png)
## Phone number verification
![Screenshot](verification-page.png)
## Home page
![Screenshot](home-page.png)
## Search Donor
![Screenshot](search-donor.png)
## Donor list
![Screenshot](donor.png)
## Donor information
![Screenshot](donor-info.png)
## Donor edit page
![Screenshot](donor-edit.png)
## Category
![Screenshot](category.png)
## User list
![Screenshot](user-list.png)
## Manage user
![Screenshot](manage-user.png)

