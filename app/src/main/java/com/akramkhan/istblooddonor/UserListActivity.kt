package com.akramkhan.istblooddonor

import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.akramkhan.istblooddonor.model.User
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.firebase.ui.database.SnapshotParser
import com.google.firebase.FirebaseApp
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_user_list.*
import kotlinx.android.synthetic.main.item_user.view.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.textColor

class UserListActivity : AppCompatActivity() {
    private var adapter: FirebaseRecyclerAdapter<*, *>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_list)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.setHasFixedSize(true)
        fetchUser()
    }

    inner class ViewHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun setName(s: String) {
            itemView.nameId.text = s
        }
        fun setMobile(s: String) {
            itemView.mobileId.text = s
        }
        fun setRole(user: User) {
            if (user.role_admin==true) {
                itemView.roleId.text = "Admin"
                itemView.roleId.textColor = Color.RED
            }
            else if(user.role_moderator==true) {
                itemView.roleId.text = "Moderator"
                itemView.roleId.textColor = Color.MAGENTA
            }
            else if(user.role_user==true){
                itemView.roleId.text = "User"
                itemView.roleId.textColor = Color.GRAY
            }
            else {
                itemView.roleId.text = "None"
                itemView.roleId.textColor = Color.LTGRAY
            }
        }
        fun setSerial(p: Int) {
            itemView.serialId.text = "${p+1}"
        }
    }
    private fun fetchUser() {
        FirebaseApp.initializeApp(this)
        val query = FirebaseDatabase.getInstance().reference.child("users")
            .orderByChild("role")
        val options = FirebaseRecyclerOptions.Builder<User>()
            .setQuery(query, object : SnapshotParser<User> {
                override fun parseSnapshot(snapshot: DataSnapshot): User {
                    return snapshot.getValue(User::class.java)!!
                }
            }).build()
        adapter = object : FirebaseRecyclerAdapter<User, ViewHolder>(options) {
            override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_user, parent, false)
                return ViewHolder(view)
            }

            override fun onBindViewHolder(holder: ViewHolder, position: Int, user: User) {
                holder.setName(user.name!!)
                holder.setMobile(user.phone!!)
                holder.setRole(user)
                holder.setSerial(position)
                holder.itemView.onClick {
                    startActivity<UserEditActivity>("uid" to user.uid)
                }
            }
        }
        recyclerView.adapter = adapter
    }

    override fun onStart() {
        super.onStart()
        adapter?.startListening()
    }

    override fun onStop() {
        super.onStop()
        adapter?.stopListening()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}
