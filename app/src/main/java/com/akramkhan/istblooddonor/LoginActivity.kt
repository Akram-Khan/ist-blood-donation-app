package com.akramkhan.istblooddonor

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.startActivity

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        continueId.onClick {
            val no = mobileId.text.toString()
            validNo(no)
            startActivity<VerifyPhoneActivity>("mobile" to no)
            finish()
        }
    }
    private fun validNo(no: String) {
        if (no.isEmpty() || no.length != 11) {
            mobileId.error = "Enter a 11 digit valid mobile number"
            mobileId.requestFocus()
            return
        }
    }
}
