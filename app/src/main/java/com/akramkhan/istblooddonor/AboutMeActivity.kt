package com.akramkhan.istblooddonor

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

class AboutMeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about_me)
    }
}
