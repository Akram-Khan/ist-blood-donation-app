package com.akramkhan.istblooddonor

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.akramkhan.istblooddonor.model.User
import com.dd.processbutton.iml.ActionProcessButton
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_user_edit.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.toast

class UserEditActivity : AppCompatActivity() {
    var roleUser = true
    var roleModerator = false
    var roleAdmin = false
    var role = "user"
    var uid = ""
    lateinit var user: User
    lateinit var app: MyApplication
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_edit)
        uid = intent.getStringExtra("uid")
        getUserByUIDAndPopulate(uid)
        clearEditText(nameId)
        clearEditText(emailId)
        updateId.onClick {
            updateUserToDatabase()
        }
        roleAdminId.onClick {
            setRole("admin", true, true, true)
        }
        roleModeratorId.onClick {
            setRole("moderator", true, true, false)
        }
        roleUserId.onClick {
            setRole("user", true, false, false)
        }
        roleNoneId.onClick {
            setRole("void", false, false, false)
        }
    }

    private fun setRole(role: String, user: Boolean, moderator: Boolean, admin: Boolean) {
        this.role = role
        roleUser = user
        roleModerator = moderator
        roleAdmin = admin
    }

    private fun updateUserToDatabase() {
        val name = nameId.text.toString()
        val email = emailId.text.toString()
        updateId.setMode(ActionProcessButton.Mode.ENDLESS)
        updateId.progress = 1
        updateId.text = "Updating to database"
        updateId.isClickable = false
        val databaseReference = FirebaseDatabase.getInstance().reference.child("users").child(uid)
        val userToUpdate = user
        userToUpdate.name = name
        userToUpdate.email = email
        userToUpdate.role = role
        userToUpdate.role_admin = roleAdmin
        userToUpdate.role_moderator = roleModerator
        userToUpdate.role_user = roleUser
        databaseReference.setValue(user)
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    toast("User updated successfully")
                }
                else {
                    toast(it.exception.toString())
                }
                updateId.progress = 0
                updateId.text = "Update"
                updateId.isClickable = true
            }
    }

    private fun getUserByUIDAndPopulate(uid: String) {
        FirebaseDatabase.getInstance().reference
            .child("users").child(uid)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {}
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    user = dataSnapshot.getValue(User::class.java)!!
                    populateUserData(user)
                }
            })
    }
    private fun populateUserData(user: User) {
        nameId.setText(user.name)
        emailId.setText(user.email)
        if (user.role_admin==true) {
            roleAdminId.isChecked = true
            setRole("admin", true, true, true)
        }
        else if(user.role_moderator==true) {
            roleModeratorId.isChecked = true
            setRole("moderator", true, true, false)
        }
        else if(user.role_user == true){
            roleUserId.isChecked = true
            setRole("user", true, false, false)
        }
        else {
            roleNoneId.isChecked = true
            setRole("void", false, false, false)
        }
    }
}
