package com.akramkhan.istblooddonor

import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.akramkhan.istblooddonor.model.Donor
import com.beardedhen.androidbootstrap.TypefaceProvider
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.firebase.ui.database.SnapshotParser
import com.google.firebase.FirebaseApp
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_donor.*
import org.jetbrains.anko.toast

class ThisMonthActivity : AppCompatActivity() {
    private var adapter: FirebaseRecyclerAdapter<*, *>? = null
    var phoneNumber = arrayListOf<String>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        TypefaceProvider.registerDefaultIconSets()
        setContentView(R.layout.activity_donor)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.setHasFixedSize(true)
        fetch()
    }


    private fun fetch() {
        FirebaseApp.initializeApp(this)
        val query = FirebaseDatabase.getInstance().reference.child("donors")
            .orderByChild("date").startAt(getFirstDateOfMonth()).endAt(getLastDateOfMonth())
        val options: FirebaseRecyclerOptions<Donor> = FirebaseRecyclerOptions.Builder<Donor>()
            .setQuery(query, object : SnapshotParser<Donor> {
                override fun parseSnapshot(snapshot: DataSnapshot): Donor {
                    val p = snapshot.getValue(Donor::class.java)
                    return p!!
                }
            }).build()
        adapter = getDonorRecyclerAdapter(phoneNumber, options, this@ThisMonthActivity)
        recyclerView.adapter = adapter
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == 100) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) callPhoneNumber(this@ThisMonthActivity, phoneNumber[0])
            else toast("Permission deny")
        }
    }

    override fun onStart() {
        super.onStart()
        adapter?.startListening()
    }

    override fun onStop() {
        super.onStop()
        adapter?.stopListening()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}
