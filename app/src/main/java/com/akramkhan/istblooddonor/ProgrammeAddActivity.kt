package com.akramkhan.istblooddonor

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.EditText
import com.akramkhan.istblooddonor.model.Programme
import com.dd.processbutton.iml.ActionProcessButton
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_add_edit_donor.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.sdk25.coroutines.onTouch
import org.jetbrains.anko.toast

class ProgrammeAddActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_programme)
        val programmes = getAllProgramme()
        programmeId.onTouch { v, _ -> clearEditText(v as EditText) }
        addId.onClick {
            val name = programmeId.text.toString().trim()
            when {
                programmes.contains(name) -> toast("Category already exists")
                name == "" -> programmeId.error = "Enter a category name"
                else -> {
                    addId.setMode(ActionProcessButton.Mode.ENDLESS)
                    addId.progress = 1
                    addId.text = "Adding to database"
                    addId.isClickable = false
                    val databaseReference = FirebaseDatabase.getInstance().reference.child("programmes").push()
                    val programme = Programme(databaseReference.key, name)
                    databaseReference.setValue(programme).addOnCompleteListener {
                        if (!it.isSuccessful) {
                            toast("Something went wrong")
                        } else {
                            addId.progress = 0
                            toast("Category added successfully")
                        }
                        addId.text = "Add"
                        addId.isClickable = true
                    }
                }
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}
