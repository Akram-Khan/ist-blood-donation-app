package com.akramkhan.istblooddonor

import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.akramkhan.istblooddonor.model.Donor
import com.beardedhen.androidbootstrap.TypefaceProvider
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_donor_view.*
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.Appcompat
import org.jetbrains.anko.sdk25.coroutines.onClick

class DonorViewActivity : AppCompatActivity() {
    var donor = Donor()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        TypefaceProvider.registerDefaultIconSets();
        setContentView(R.layout.activity_donor_view)
        val app = application as MyApplication
        if (app.user.role_moderator==false) {
            updateDeleteLayoutId.visibility = View.GONE
        }
        if (app.user.role_moderator == true && app.user.role_admin == false){
            deleteId.visibility = View.GONE
        }
        getData()
        updateId.onClick {
            startActivity<DonorEditActivity>("key" to donor.id)
        }
        deleteId.onClick {
            alert(Appcompat, "Are you sure to delete?") {
                yesButton { FirebaseDatabase.getInstance()
                    .reference
                    .child("donors")
                    .child(intent.getStringExtra("key")).setValue(null)
                    .addOnCompleteListener {
                        if (it.isSuccessful) {
                            toast("Deleted successfully")
                            finish()
                        }
                    }
                }
                noButton { it.dismiss() }
            }.show()
        }
        callId.onClick {
            callPhoneNumber(this@DonorViewActivity, donor.mobile)
        }
        messageId.onClick {
            sendSMS(donor.mobile)
        }
    }

    private fun getData() {
        FirebaseDatabase.getInstance()
            .reference
            .child("donors")
            .child(intent.getStringExtra("key"))
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {}
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    donor = dataSnapshot.getValue(Donor::class.java)!!
                    populateData(donor)
                }
            })
    }

    private fun populateData(donor: Donor) {
        nameId.text = donor.name
        addressId.text = donor.address
        mobileId.text = donor.mobile
        bloodId.text = donor.bloodGroup
        ageId.text = donor.age
        weightId.text = donor.weight
        programmeId.text = donor.programme
        commentId.text = donor.comment
        donationNumberId.text = donor.donationNumber
        dateId.text = donor.date
    }

    override fun onResume() {
        super.onResume()
        getData()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == 100) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) callPhoneNumber(this@DonorViewActivity, donor.mobile)
            else toast("Permission deny")
        }
    }
}
