package com.akramkhan.istblooddonor

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.akramkhan.istblooddonor.model.Donor
import com.dd.processbutton.iml.ActionProcessButton
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_add_edit_donor.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.toast


class DonorEditActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_edit_donor)
        val allProgramme = getAllProgramme()
        addId.text = "Update"
        bloodGroupId.onClick {
            populatePopupForSelect(
                this@DonorEditActivity,
                arrayListOf("A+", "A-", "B+", "B-", "AB+", "AB-", "O+", "O-"),
                bloodGroupId,
                "Select Blood Group"
            )
        }
        commentId.onClick {
            populatePopupForSelect(
                this@DonorEditActivity,
                arrayListOf("Ready to Donate", "Medium Chance", "Low Chance", "No Chance"),
                commentId,
                "Select Comment"
            )
        }
        programmeId.onClick {
            populatePopupForSelect(
                this@DonorEditActivity,
                allProgramme,
                programmeId,
                "Select Category"
            )
        }
        dateId.setOnClickListener { populateDatePicker(this,  dateId) }
        getPersonByIdAndPopulate(intent.getStringExtra("key"), this)
        addId.onClick { updateToDatabase() }
        clearDonorEditTextByTouch(this)
    }

    private fun updateToDatabase() {
        val name = nameId.text.toString().trim()
        val address = addressId.text.toString().trim()
        val mobile = mobileNumberId.text.toString().trim()
        val blood = bloodGroupId.text.toString().trim()
        val age = ageId.text.toString().trim()
        val weight =weightId.text.toString().trim()
        val programme = programmeId.text.toString().trim()
        val comment = commentId.text.toString().trim()
        val donationNumber = donationNumberId.text.toString().trim()
        val date = dateId.text.toString().trim()

        when {
            name == "" -> nameId.error = "Enter your name"
            address == "" -> addressId.error = "Enter your address"
            mobile == "" -> mobileNumberId.error = "Enter your mobile number"
            blood == "" -> addressId.error = "Select your blood group"
            age == "" -> ageId.error = "Enter your age"
            weight == "" -> weightId.error = "Enter your weight"
            programme == "" -> programmeId.error = "Select a Category"
            else -> {
                addId.setMode(ActionProcessButton.Mode.ENDLESS)
                addId.progress = 1
                addId.text = "Updating to database"
                addId.isClickable = false
                val id = intent.getStringExtra("key")
                val databaseReference = FirebaseDatabase.getInstance().reference.child("donors").child(id)
                val person = Donor(id, name, address, mobile, blood, age, weight, programme, comment, donationNumber, date)
                databaseReference.setValue(person).addOnCompleteListener {
                    if (!it.isSuccessful) {
                        toast("Something went wrong")
                        System.out.println(it)
                    } else {
                        toast("Information updated successfully")
                    }
                    addId.progress = 0
                    addId.text = "Update"
                    addId.isClickable = true
                }
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}
