package com.akramkhan.istblooddonor

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.google.android.gms.tasks.TaskExecutors
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import kotlinx.android.synthetic.main.activity_verify_phone.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import java.util.concurrent.TimeUnit


class VerifyPhoneActivity : AppCompatActivity() {
    lateinit var verificationId: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verify_phone)
        sendVerificationCode(intent.getStringExtra("mobile"))
        verificationId = ""
        loginId.onClick {
            val code = codeId.text.toString()
            if (code.isEmpty() || code.length < 6) {
                codeId.error = "Enter a valid code"
                codeId.requestFocus()
                return@onClick
            }
            verifyVerificationCode(code)
        }
    }

    private fun sendVerificationCode(no: String) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
            "+88" + no,
            60,
            TimeUnit.SECONDS,
            TaskExecutors.MAIN_THREAD, callbacks
        )
    }

    val callbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        override fun onVerificationCompleted(phoneAuthCredential: PhoneAuthCredential) {
            val code = phoneAuthCredential.smsCode
            if (code != null) {
                codeId.setText(code)
                verifyVerificationCode(code)
            }
        }

        override fun onCodeSent(s: String, forceResendingToken: PhoneAuthProvider.ForceResendingToken) {
            super.onCodeSent(s, forceResendingToken)
            verificationId = s
        }
        override fun onVerificationFailed(e: FirebaseException) {
            toast(e.message.toString())
            System.out.println(e.message)
        }

    }

    private fun verifyVerificationCode(code: String) {
        val credential = PhoneAuthProvider.getCredential(verificationId, code)
        signInWithPhoneAuthCredential(credential)
    }

    private fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential) {
        FirebaseAuth.getInstance().signInWithCredential(credential)
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    startActivity<MainActivity>()
                    finish()
                }
                else {
                    var message = "Somthing is wrong, we will fix it soon..."

                    if (it.exception is FirebaseAuthInvalidCredentialsException) {
                        message = "Invalid code entered..."
                    }
                    toast(message)
                }
            }
    }
}
