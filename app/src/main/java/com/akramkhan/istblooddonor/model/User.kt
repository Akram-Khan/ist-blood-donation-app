package com.akramkhan.istblooddonor.model

class User {
    var uid: String? = null
    var email: String? = null
    var phone: String? = null
    var name: String? = null
    var role: String? = null
    var role_user: Boolean? = null
    var role_moderator: Boolean? = null
    var role_admin: Boolean? = null

    constructor() {}

    constructor(uid: String, email: String, phone: String, name: String, role: String, role_user: Boolean, role_moderator: Boolean, role_admin: Boolean) {
        this.uid = uid
        this.email = email
        this.phone = phone
        this.name = name
        this.role = role
        this.role_user = role_user
        this.role_moderator = role_moderator
        this.role_admin = role_admin
    }
}
