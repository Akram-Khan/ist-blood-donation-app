package com.akramkhan.istblooddonor.model;

public class Donor {
    private String id, name, address, mobile, bloodGroup, age, weight, programme, comment, donationNumber, date;

    public Donor() {
    }

    public Donor(String id, String name, String address, String mobile, String bloodGroup, String age, String weight, String programme, String comment, String donationNumber, String date) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.mobile = mobile;
        this.bloodGroup = bloodGroup;
        this.age = age;
        this.weight = weight;
        this.programme = programme;
        this.comment = comment;
        this.donationNumber = donationNumber;
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProgramme() {
        return programme;
    }

    public void setProgramme(String programme) {
        this.programme = programme;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDonationNumber() {
        return donationNumber;
    }

    public void setDonationNumber(String donationNumber) {
        this.donationNumber = donationNumber;
    }
}
