package com.akramkhan.istblooddonor

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.widget.PopupWindow
import com.akramkhan.istblooddonor.model.User
import com.beardedhen.androidbootstrap.TypefaceProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.popup_search.view.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.startActivity


class MainActivity : AppCompatActivity() {
    private var popupWindow: PopupWindow? = null
    private var allProgramme = arrayListOf<String>()
    lateinit var firebaseAuth: FirebaseAuth
    var uid = "null"
    lateinit var authStateListener: FirebaseAuth.AuthStateListener
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        firebaseAuth = FirebaseAuth.getInstance()
        if (firebaseAuth.currentUser != null) {
            uid = firebaseAuth.currentUser!!.uid
        }
        authStateListener = FirebaseAuth.AuthStateListener {
            if (firebaseAuth.currentUser == null){
                startActivity<LoginActivity>()
                finish()
            }
            else {
                uid = firebaseAuth.currentUser!!.uid
            }
        }
        TypefaceProvider.registerDefaultIconSets()
        setContentView(R.layout.activity_main)
        val dialog = ProgressDialog.show(
            this@MainActivity, "",
            "Loading. Please wait...", true
        )
        val app = application as MyApplication
        val databaseReference = FirebaseDatabase.getInstance().reference.child("users").child(uid)
        databaseReference.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {}
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                app.user = dataSnapshot.getValue(User::class.java)
                val user = User(uid, "email", firebaseAuth.currentUser!!.phoneNumber!!, "name", "user", true, false, false)
                if (app.user==null) databaseReference.setValue(user)
                if (app.user != null){
                    if(app.user.role_moderator == false) {
                        addId.visibility = View.GONE
                    }
                    else {
                        addId.visibility = View.VISIBLE
                    }
                }
                if (app.user != null) {
                    dialog.dismiss()
                    mainLayout.visibility = View.VISIBLE
                }
                if (app.user!=null && app.user.role_admin == true) {
                    userManagementId.visibility = View.VISIBLE
                }
                else {
                    userManagementId.visibility = View.GONE
                }
            }
        })
        allProgramme = getAllProgramme()
        logoutId.onClick { firebaseAuth.signOut() }
        searchId.onClick { populatePopupForSearch() }
        addId.onClick { startActivity<DonorAddActivity>() }
        programmeId.onClick { startActivity<ProgrammeActivity>()}
        donationOfThisMonth.onClick { startActivity<ThisMonthActivity>() }
        userManagementId.onClick { startActivity<UserListActivity>() }
    }


    @SuppressLint("InflateParams")
    private fun populatePopupForSearch() {
        mainLayout.setBackgroundColor(Color.GRAY)
        val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.popup_search, null)
        popupWindow = getPopupWindow(mainLayout, view, 1)
        view.nameSubmitId.onClick {
            startActivity<DonorListActivity>("key" to "name", "value" to view.nameEditId.text.toString().trim())
        }
        view.programmeId.onClick {
            view.inputLayout.visibility = View.GONE
            view.list.visibility = View.VISIBLE
            populateProgrammeOrBloodGroup(this@MainActivity, view.list, "programme", allProgramme)
        }
        view.bloodGroupId.onClick {
            view.inputLayout.visibility = View.GONE
            view.list.visibility = View.VISIBLE
            populateProgrammeOrBloodGroup(
                this@MainActivity, view.list, "bloodGroup",
                arrayListOf("A+", "A-", "B+", "B-", "AB+", "AB-", "O+", "O-")
            )
        }
        view.nameId.onClick {
            view.list.visibility = View.GONE
            view.inputLayout.visibility = View.VISIBLE
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        popupWindow?.dismiss()
    }

    override fun onStart() {
        super.onStart()
        firebaseAuth.addAuthStateListener(authStateListener)
    }

    override fun onStop() {
        super.onStop()
        firebaseAuth.removeAuthStateListener(authStateListener)
    }
}
