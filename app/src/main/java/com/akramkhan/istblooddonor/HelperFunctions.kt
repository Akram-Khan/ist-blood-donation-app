package com.akramkhan.istblooddonor

import android.Manifest
import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.os.Build
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.PopupWindow
import android.widget.TextView
import com.akramkhan.istblooddonor.model.Donor
import com.akramkhan.istblooddonor.model.Programme
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_add_edit_donor.*
import kotlinx.android.synthetic.main.item_donor.view.*
import kotlinx.android.synthetic.main.popup_select.view.*
import org.jetbrains.anko.makeCall
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.sdk25.coroutines.onTouch
import java.text.SimpleDateFormat
import java.util.*

fun getAllProgramme(): ArrayList<String> {
    val allProgramme = arrayListOf<String>()
    FirebaseDatabase.getInstance().reference.child("programmes")
        .addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {}
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                allProgramme.clear()
                dataSnapshot.children.forEach {
                    val p = it.getValue(Programme::class.java)
                    allProgramme.add(p!!.name)
                }
            }
        })
    return allProgramme
}


fun populateDatePicker(context: Context, dateEditText: EditText) {
    val calendar = Calendar.getInstance()!!
    val datePicker = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
        calendar.set(Calendar.YEAR, year)
        calendar.set(Calendar.MONTH, monthOfYear)
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        dateEditText.setText(SimpleDateFormat("yyyy.MM.dd", Locale.US).format(calendar.time))
    }
    DatePickerDialog(
        context, datePicker,
        calendar.get(Calendar.YEAR),
        calendar.get(Calendar.MONTH),
        calendar.get(Calendar.DAY_OF_MONTH)
    ).show()
}
fun getFirstDateOfMonth(): String {
    val date = Calendar.getInstance()
    date.set(Calendar.DAY_OF_MONTH, 1)
    return SimpleDateFormat("yyyy.MM.dd", Locale.US).format(date.time)
}
fun getLastDateOfMonth(): String {
    val date = Calendar.getInstance()
    date.add(Calendar.MONTH, 1)
    date.set(Calendar.DAY_OF_MONTH, 1)
    date.add(Calendar.DATE, -1)
    return SimpleDateFormat("yyyy.MM.dd", Locale.US).format(date.time)
}
fun clearDonorEditTextByTouch(activity: AppCompatActivity) {
    clearEditText(activity.nameId)
    clearEditText(activity.bloodGroupId)
    clearEditText(activity.ageId)
    clearEditText(activity.weightId)
    clearEditText(activity.addressId)
    clearEditText(activity.programmeId)
    clearEditText(activity.mobileNumberId)
    clearEditText(activity.commentId)
    clearEditText(activity.dateId)
}

fun clearEditText(editText: EditText) {
    editText.onTouch { _, event ->
        if (event.action == MotionEvent.ACTION_UP) {
            val drawableRight = 2
            if (event.rawX >= (editText.right - editText.compoundDrawables[drawableRight].bounds.width())) {
                editText.setText("")
            }
        }
    }
}
fun populatePersonData(donor: Donor, activity: AppCompatActivity) {
    activity.nameId.setText(donor.name)
    activity.addressId.setText(donor.address)
    activity.mobileNumberId.setText(donor.mobile)
    activity.bloodGroupId.setText(donor.bloodGroup)
    activity.ageId.setText(donor.age)
    activity.weightId.setText(donor.weight)
    activity.programmeId.setText(donor.programme)
    activity.commentId.setText(donor.comment)
    activity.donationNumberId.setText(donor.donationNumber)
    activity.dateId.setText(donor.date)
}


fun getPersonByIdAndPopulate(id: String, activity: AppCompatActivity) {
    FirebaseDatabase.getInstance()
        .reference
        .child("donors")
        .child(id)
        .addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {}
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val person = dataSnapshot.getValue(Donor::class.java)!!
                populatePersonData(person, activity)
            }
        })
}

fun populateProgrammeOrBloodGroup(activity: AppCompatActivity, recyclerView: RecyclerView, key: String, list: ArrayList<String>, editText: EditText? = null, popupWindow: PopupWindow? = null) {
    class ViewHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun setName(s: String){
            val name = itemView.findViewById<TextView>(R.id.nameId)
            name.text = s
        }
    }
    recyclerView.layoutManager = LinearLayoutManager(activity)
    val adapter = object : RecyclerView.Adapter<ViewHolder>() {
        override fun onBindViewHolder(holder: ViewHolder, index: Int) {
            holder.setName(list[index])
            holder.itemView.onClick {
                if (editText==null) {
                    activity.startActivity<DonorListActivity>("key" to key, "value" to list[index])
                }
                else {
                    editText.setText(list[index])
                    popupWindow?.dismiss()
                }
            }
        }
        override fun getItemCount(): Int {
            return list.size
        }
        override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_select_programme_or_blood_group, parent, false)
            return ViewHolder(view)
        }
    }
    recyclerView.adapter = adapter
}

@Suppress("DEPRECATION")
fun getPopupWindow(rootLayout: View, view: View, flag: Int): PopupWindow? {
    val popupWindow = PopupWindow(
        view,
        LinearLayout.LayoutParams.WRAP_CONTENT,
        LinearLayout.LayoutParams.WRAP_CONTENT,
        true
    )
    popupWindow.setBackgroundDrawable(object : BitmapDrawable() {})
    popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0)
    popupWindow.setOnDismissListener {
        if (flag==1) {
            rootLayout.setBackgroundColor(Color.parseColor("#8b0000"))
        }
        else {
            rootLayout.setBackgroundColor(Color.WHITE)
        }
    }
    return popupWindow
}

@SuppressLint("InflateParams")
fun populatePopupForSelect(activity: AppCompatActivity, list: ArrayList<String>, editText: EditText, s: String) {
    activity.mainLayout.setBackgroundColor(Color.GRAY)
    val inflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    val view = inflater.inflate(R.layout.popup_select, null)
    val tv = view.findViewById<TextView>(R.id.tv)
    tv.text = s
    val popupWindow = getPopupWindow(activity.mainLayout, view, 0)
    populateProgrammeOrBloodGroup(activity, view.recyclerView, "", list, editText, popupWindow)
}

fun callPhoneNumber(activity: AppCompatActivity, phoneNumber: String) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.CALL_PHONE), 100)
        }
    }
    activity.makeCall(phoneNumber)
}


fun getDonorRecyclerAdapter(
    phoneNumber: ArrayList<String>,
    options: FirebaseRecyclerOptions<Donor>,
    activity: AppCompatActivity
): FirebaseRecyclerAdapter<*, *>? {
    class ViewHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun setName(s: String){
            val name = itemView.findViewById<TextView>(R.id.nameId)
            name.text = s
        }
        fun setBlood(s: String) {
            itemView.bloodId.text = s
        }
        fun setAge(s: String) {
            itemView.ageId.text = s
        }
        fun setAddress(s: String) {
            itemView.addressId.text = s
        }
        fun setMobile(s: String) {
            itemView.mobileId.text = s
        }
        fun setComment(s: String) {
            itemView.commentId.text = s
        }
        fun setDate(s: String) {
            itemView.dateId.text = s
        }
        fun setSerial(p: Int) {
            itemView.serialId.text = "${p+1}"
        }
    }
    return object : FirebaseRecyclerAdapter<Donor, ViewHolder>(options) {
        override fun onBindViewHolder(holder: ViewHolder, position: Int, donor: Donor) {
            holder.setName(donor.name)
            holder.setBlood(donor.bloodGroup)
            holder.setAge(donor.age)
            holder.setAddress(donor.address)
            holder.setMobile(donor.mobile)
            holder.setComment(donor.comment)
            holder.setDate(donor.date)
            holder.setSerial(position)
            holder.itemView.callId.onClick {
                phoneNumber.clear()
                phoneNumber.add(donor.mobile)
                callPhoneNumber(activity, donor.mobile)
            }
            holder.itemView.onClick {
                activity.startActivity<DonorViewActivity>("key" to donor.id)
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, i: Int): ViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_donor, parent, false)
            return ViewHolder(view)
        }
    }
}