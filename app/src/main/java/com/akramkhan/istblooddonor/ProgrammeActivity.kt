package com.akramkhan.istblooddonor

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.akramkhan.istblooddonor.model.Programme
import com.beardedhen.androidbootstrap.TypefaceProvider
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.firebase.ui.database.SnapshotParser
import com.google.firebase.FirebaseApp
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_add_programme.*
import kotlinx.android.synthetic.main.activity_programme.*
import kotlinx.android.synthetic.main.item_programme.*
import kotlinx.android.synthetic.main.item_programme.view.*
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.Appcompat
import org.jetbrains.anko.sdk25.coroutines.onClick


class ProgrammeActivity : AppCompatActivity() {
    private var adapter: FirebaseRecyclerAdapter<*, *>? = null
    lateinit var app: MyApplication
    val map = HashMap<String, String>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        TypefaceProvider.registerDefaultIconSets()
        setContentView(R.layout.activity_programme)
        app = application as MyApplication
        if (app.user.role_admin==false) {
            addNewId.visibility = View.GONE
        }
        addNewId.onClick {
            startActivity<ProgrammeAddActivity>()
        }
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.setHasFixedSize(true)
        fetchProgramme()
    }

    inner class ViewHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun setName(s: String){
            itemView.nameId.text = s
        }
        fun hideDeleteButton() {
            itemView.deleteId.visibility = View.GONE
        }
    }
    private fun fetchProgramme() {
        FirebaseApp.initializeApp(this)
        val query = FirebaseDatabase.getInstance()
            .reference
            .child("programmes")
        val options = FirebaseRecyclerOptions.Builder<Programme>()
            .setQuery(query, object : SnapshotParser<Programme> {
                override fun parseSnapshot(snapshot: DataSnapshot): Programme {
                    val p = snapshot.getValue(Programme::class.java)
                    map[p!!.name] = p.id
                    return p
                }
            }).build()
        adapter = object : FirebaseRecyclerAdapter<Programme, ViewHolder>(options) {
            override fun onBindViewHolder(holder: ViewHolder, position: Int, programme: Programme) {
                holder.setName(programme.name)
                if (app.user.role_admin==false) {
                    holder.hideDeleteButton()
                }
                holder.itemView.onClick {
                    startActivity<DonorListActivity>("key" to "programme", "value" to programme.name)
                }
                holder.itemView.deleteId.onClick {
                    alert(Appcompat, "Are you sure to delete?") {
                        yesButton { FirebaseDatabase.getInstance()
                            .reference
                            .child("programmes")
                            .child(map[programme.name]!!).setValue(null)
                            .addOnCompleteListener {
                                if (it.isSuccessful) {
                                    toast("Deleted successfully")
                                    adapter?.startListening()
                                }
                            }
                        }
                        noButton { it.dismiss() }
                    }.show()
                }
            }

            override fun onCreateViewHolder(parent: ViewGroup, i: Int): ViewHolder {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_programme, parent, false)
                return ViewHolder(view)
            }
        }
        recyclerView.adapter = adapter
    }
    override fun onStart() {
        super.onStart()
        adapter?.startListening()
    }
    override fun onStop() {
        super.onStop()
        adapter?.stopListening()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}

